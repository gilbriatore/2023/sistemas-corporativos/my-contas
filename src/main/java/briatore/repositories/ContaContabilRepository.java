package briatore.repositories;

import briatore.entities.ContaContabil;
import briatore.entities.TipoConta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaContabilRepository extends JpaRepository<ContaContabil, Long> {

    List<ContaContabil> findByTipoIn(List<TipoConta> tipos);

    ContaContabil findByCodigo(String codigo);
}
