package briatore.repositories;

import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoLancamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LancamentoContabilRepository extends JpaRepository<LancamentoContabil, Long> {
    List<LancamentoContabil> findByConta(ContaContabil conta);
    List<LancamentoContabil> findByTipo(TipoLancamento tipoLancamento);
    List<LancamentoContabil> findByTipoAndContaIn(TipoLancamento tipoLancamento, List<ContaContabil> contas);
}
