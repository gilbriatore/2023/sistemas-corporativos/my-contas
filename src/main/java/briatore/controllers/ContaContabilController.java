package briatore.controllers;

import briatore.dtos.ContaContabilDTO;
import briatore.dtos.DRE;
import briatore.dtos.LancamentoDTO;
import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoConta;
import briatore.entities.TipoLancamento;
import briatore.repositories.ContaContabilRepository;
import briatore.repositories.LancamentoContabilRepository;
import briatore.services.BalanceteService;
import briatore.services.DreService;
import briatore.services.LancamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/contas")
public class ContaContabilController {
    @Autowired
    private ContaContabilRepository contaRepository;

    @Autowired
    private LancamentoContabilRepository lancamentoRepository;

    @Autowired
    LancamentoService lancamentoService = new LancamentoService();

    @Autowired
    DreService dreService = new DreService();

    @Autowired
    BalanceteService balanceteService = new BalanceteService();

    @GetMapping
    public List<ContaContabil> listarContas() {
        return contaRepository.findAll();
    }

    @GetMapping("/patrimonio")
    public List<ContaContabil> listarContasPatrimonio() {
        return contaRepository.findByTipoIn(Arrays.asList(TipoConta.ATIVO, TipoConta.PASSIVO));
    }

    @GetMapping("/resultados")
    public List<ContaContabil> listarContasResultados() {
        return contaRepository.findByTipoIn(Arrays.asList(TipoConta.DESPESA, TipoConta.RECEITA));
    }


    @PostMapping
    public ContaContabil criarConta(@RequestBody ContaContabil conta) {
        return contaRepository.save(conta);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContaContabil> buscarContaPorId(@PathVariable Long id) {
        Optional<ContaContabil> optionalConta = contaRepository.findById(id);
        if (optionalConta.isPresent()) {
            return ResponseEntity.ok(optionalConta.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ContaContabil> atualizarConta(@PathVariable Long id, @RequestBody ContaContabil conta) {
        Optional<ContaContabil> optionalConta = contaRepository.findById(id);
        if (optionalConta.isPresent()) {
            conta.setId(id);
            return ResponseEntity.ok(contaRepository.save(conta));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> excluirConta(@PathVariable Long id) {
        Optional<ContaContabil> optionalConta = contaRepository.findById(id);
        if (optionalConta.isPresent()) {
            contaRepository.delete(optionalConta.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{idOrigem}/debito/{idDestino}/credito")
    public ResponseEntity<LancamentoDTO> lancarDebito(@PathVariable Long idOrigem, @PathVariable Long idDestino, @RequestBody String valor) {
        return ResponseEntity.ok(lancamentoService.lancar(idOrigem, TipoLancamento.DEBITO,
                idDestino, TipoLancamento.CREDITO, Double.parseDouble(valor)));
    }

    @PostMapping("/{idOrigem}/credito/{idDestino}/debito")
    public ResponseEntity<LancamentoDTO> lancarCredito(@PathVariable Long idOrigem, @PathVariable Long idDestino, @RequestBody String valor) {
        return ResponseEntity.ok(lancamentoService.lancar(idOrigem, TipoLancamento.CREDITO,
                idDestino, TipoLancamento.DEBITO, Double.parseDouble(valor)));
    }

    @GetMapping("/balancete")
    public List<ContaContabilDTO> getBalancete() {
        List<ContaContabilDTO> balancete = new ArrayList<>();

        Map<ContaContabil, Double> mapContas = balanceteService.calcularSaldos();
        // Para cada conta contábil, calcula o saldo
        for (ContaContabil contaContabil : mapContas.keySet()) {
            double saldo = mapContas.get(contaContabil);

            ContaContabilDTO dto = new ContaContabilDTO();
            dto.setContaContabil(contaContabil);
            dto.setSaldo(saldo);

            balancete.add(dto);
        }

        return balancete;
    }


    @GetMapping("/dre")
    public DRE getDRE() {
        DRE dre = new DRE();

        // Calcula o total de receitas
        double totalReceitas = dreService.calcularReceitas();
        dre.setReceitas(totalReceitas);

        // Calcula o total de despesas
        double totalDespesas = dreService.calcularDespesas();
        dre.setDespesas(totalDespesas);

        // Calcula o resultado (receitas - despesas)
        double resultado = dreService.calcularResultadoLiquido();
        dre.setResultado(resultado);
        return dre;
    }
}