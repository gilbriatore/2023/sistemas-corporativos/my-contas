package briatore.services;


import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoConta;
import briatore.entities.TipoLancamento;
import briatore.repositories.ContaContabilRepository;
import briatore.repositories.LancamentoContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
@Component
public class CargaInicial implements CommandLineRunner {

    @Autowired
    ContaContabilRepository contaRepository;

    @Autowired
    LancamentoContabilRepository lancamentoRepository;

    @Autowired
    LancamentoService lancamentoService;

    @Override
    public void run(String... args) throws Exception {

        cargaInicial();
        efetuarLancamentos();

    }

    private void efetuarLancamentos() {
        //Lançamento 1. Venda, a vista, de um bem imóvel no valor de R$ 50.000,00;
        ContaContabil contaCaixa = contaRepository.findByCodigo("1.1.1");
        ContaContabil contaBens = contaRepository.findByCodigo("1.2.2");
        lancamentoService.lancar(contaCaixa, TipoLancamento.DEBITO, contaBens, TipoLancamento.CREDITO, 50000.00);

        //Lançamento 2. Compra de outro imóvel com entrada de R$ 45.000,00 e financiamento de R$ 88.000,00;
        ContaContabil contasApagar = contaRepository.findByCodigo("2.1.3");
        lancamentoService.lancar(contaCaixa, TipoLancamento.CREDITO, contaBens, TipoLancamento.DEBITO, 45000.00);
        lancamentoService.lancar(contasApagar, TipoLancamento.CREDITO, contaBens, TipoLancamento.DEBITO, 88000.00);

        //Lançamento 3. Pagamento de salários no valor de R$ 15.000,00;
        ContaContabil contaSalarios = contaRepository.findByCodigo("3.1");
        ContaContabil contaBancaria = contaRepository.findByCodigo("1.1.1.03");
        lancamentoService.lancar(contaSalarios, TipoLancamento.DEBITO, contaBancaria, TipoLancamento.CREDITO, 15000.00);

        //Lançamento 4. Compra de produtos para estoque no valor de R$ 32.000,00
        ContaContabil contaEstoque = contaRepository.findByCodigo("1.1.4");
        lancamentoService.lancar(contaEstoque, TipoLancamento.DEBITO, contasApagar, TipoLancamento.CREDITO, 32000.00);

        //Lançamento 5. Venda, a prazo, de produtos no valor de R$ 22.000,00
        ContaContabil contaVendas = contaRepository.findByCodigo("4.2");
        ContaContabil contasReceber = contaRepository.findByCodigo("1.1.2");
        ContaContabil contaCustosProdutos = contaRepository.findByCodigo("3.x.x");
        lancamentoService.lancar(contaVendas, TipoLancamento.CREDITO, contasReceber, TipoLancamento.DEBITO, 22000.00);
        lancamentoService.lancar(contaEstoque, TipoLancamento.CREDITO, contaCustosProdutos, TipoLancamento.DEBITO, 1.00);

        //Lançamento 6. Recebimento, a vista, de vendas no valor de R$ 11.000,00
        lancamentoService.lancar(contaCaixa, TipoLancamento.DEBITO, contasReceber, TipoLancamento.CREDITO, 11000.00);
    }

    private void cargaInicial() {
        contaRepository.deleteAll();
        lancamentoRepository.deleteAll();

        //CONTAS DO ATIVO
        //Conta caixa
        ContaContabil conta1 = new ContaContabil();
        conta1.setCodigo("1.1.1");
        conta1.setDescricao("Caixa");
        conta1.setTipo(TipoConta.ATIVO);
        contaRepository.save(conta1);

        //Lançamento inicial da conta caixa
        LancamentoContabil lancamento1 = new LancamentoContabil();
        lancamento1.setConta(conta1);
        lancamento1.setValor(3700.00);
        lancamento1.setTipo(TipoLancamento.DEBITO);
        lancamentoRepository.save(lancamento1);

        //Conta bancária
        ContaContabil conta2 = new ContaContabil();
        conta2.setCodigo("1.1.1.03");
        conta2.setDescricao("Conta bancária");
        conta2.setTipo(TipoConta.ATIVO);
        contaRepository.save(conta2);

        //Lançamento inicial da conta bancária
        LancamentoContabil lancamento2 = new LancamentoContabil();
        lancamento2.setConta(conta2);
        lancamento2.setValor(28350.00);
        lancamento2.setTipo(TipoLancamento.DEBITO);
        lancamentoRepository.save(lancamento2);

        //Conta a receber
        ContaContabil conta3 = new ContaContabil();
        conta3.setCodigo("1.1.2");
        conta3.setDescricao("Conta a receber");
        conta3.setTipo(TipoConta.ATIVO);
        contaRepository.save(conta3);

        //Lançamento inicial da conta a receber
        LancamentoContabil lancamento3 = new LancamentoContabil();
        lancamento3.setConta(conta3);
        lancamento3.setValor(12450.00);
        lancamento3.setTipo(TipoLancamento.DEBITO);
        lancamentoRepository.save(lancamento3);

        //Estoque
        ContaContabil conta4 = new ContaContabil();
        conta4.setCodigo("1.1.4");
        conta4.setDescricao("Estoque");
        conta4.setTipo(TipoConta.ATIVO);
        contaRepository.save(conta4);

        //Lançamento inicial da conta a receber
        LancamentoContabil lancamento4 = new LancamentoContabil();
        lancamento4.setConta(conta4);
        lancamento4.setValor(11300.00);
        lancamento4.setTipo(TipoLancamento.DEBITO);
        lancamentoRepository.save(lancamento4);

        //Bens
        ContaContabil conta5 = new ContaContabil();
        conta5.setCodigo("1.2.2");
        conta5.setDescricao("Bens");
        conta5.setTipo(TipoConta.ATIVO);
        contaRepository.save(conta5);

        //Lançamento inicial da conta bens
        LancamentoContabil lancamento5 = new LancamentoContabil();
        lancamento5.setConta(conta5);
        lancamento5.setValor(67000.00);
        lancamento5.setTipo(TipoLancamento.DEBITO);
        lancamentoRepository.save(lancamento5);

        //CONTAS DO PASSIVO
        //Contas a pagar
        ContaContabil conta6 = new ContaContabil();
        conta6.setCodigo("2.1.3");
        conta6.setDescricao("Contas a pagar");
        conta6.setTipo(TipoConta.PASSIVO);
        contaRepository.save(conta6);

        //Lançamento inicial da contas a pagar
        LancamentoContabil lancamento6 = new LancamentoContabil();
        lancamento6.setConta(conta6);
        lancamento6.setValor(39120.00);
        lancamento6.setTipo(TipoLancamento.CREDITO);
        lancamentoRepository.save(lancamento6);

        //Capital social
        ContaContabil conta7 = new ContaContabil();
        conta7.setCodigo("2.3.1");
        conta7.setDescricao("Capital Social");
        conta7.setTipo(TipoConta.PASSIVO);
        contaRepository.save(conta7);

        //Lançamento inicial da conta capital social
        LancamentoContabil lancamento7 = new LancamentoContabil();
        lancamento7.setConta(conta7);
        lancamento7.setValor(70000.00);
        lancamento7.setTipo(TipoLancamento.CREDITO);
        lancamentoRepository.save(lancamento7);

        //Conta resultados
        ContaContabil conta8 = new ContaContabil();
        conta8.setCodigo("2.3.1.1.01");
        conta8.setDescricao("Resultados");
        conta8.setTipo(TipoConta.PASSIVO);
        contaRepository.save(conta8);

        //Lançamento inicial da conta resultados
        LancamentoContabil lancamento8 = new LancamentoContabil();
        lancamento8.setConta(conta8);
        lancamento8.setValor(13680.00);
        lancamento8.setTipo(TipoLancamento.CREDITO);
        lancamentoRepository.save(lancamento8);

        //CONTAS DE RESULTADOS
        //Pessoal e encargos
        ContaContabil conta9 = new ContaContabil();
        conta9.setCodigo("3.1");
        conta9.setDescricao("Pessoal e encargos");
        conta9.setTipo(TipoConta.DESPESA);
        contaRepository.save(conta9);

        //Custo de produtos e serviços
        ContaContabil conta14 = new ContaContabil();
        conta14.setCodigo("3.x.x");
        conta14.setDescricao("Custo de produtos e serviços");
        conta14.setTipo(TipoConta.DESPESA);
        contaRepository.save(conta14);

        //Consumo de produtos e serviços
        ContaContabil conta10 = new ContaContabil();
        conta10.setCodigo("3.3");
        conta10.setDescricao("Consumo de produtos e serviços");
        conta10.setTipo(TipoConta.DESPESA);
        contaRepository.save(conta10);

        //Despesas financeiras
        ContaContabil conta11 = new ContaContabil();
        conta11.setCodigo("3.4");
        conta11.setDescricao("Despesas financeiras");
        conta11.setTipo(TipoConta.DESPESA);
        contaRepository.save(conta11);

        //CONTAS DE RECEITAS
        //Vendas de produtos e serviços
        ContaContabil conta12 = new ContaContabil();
        conta12.setCodigo("4.2");
        conta12.setDescricao("Vendas de produtos e serviços");
        conta12.setTipo(TipoConta.RECEITA);
        contaRepository.save(conta12);

        //Receitas financeiras
        ContaContabil conta13 = new ContaContabil();
        conta13.setCodigo("4.3");
        conta13.setDescricao("Receitas financeiras");
        conta13.setTipo(TipoConta.RECEITA);
        contaRepository.save(conta13);
    }
}
