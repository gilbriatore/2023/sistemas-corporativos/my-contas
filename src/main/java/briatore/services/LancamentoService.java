package briatore.services;

import briatore.dtos.LancamentoDTO;
import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoLancamento;
import briatore.repositories.ContaContabilRepository;
import briatore.repositories.LancamentoContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LancamentoService {

    @Autowired
    private ContaContabilRepository contaRepository;

    @Autowired
    private LancamentoContabilRepository lancamentoRepository;

    public LancamentoDTO lancar(Long idOrigem, TipoLancamento tipoOrigem,
                                Long idDestino, TipoLancamento tipoDestino,
                                double valor) {
        LancamentoDTO lancamentoDTO = new LancamentoDTO();
        Optional<ContaContabil> optionalContaOrigem = contaRepository.findById(idOrigem);
        Optional<ContaContabil> optionalContaDestino = contaRepository.findById(idDestino);

        if (optionalContaOrigem.isPresent() && optionalContaDestino.isPresent()) {
            lancamentoDTO = lancar(optionalContaOrigem.get(), tipoOrigem,
                    optionalContaDestino.get(), tipoDestino, valor);
        }
        return lancamentoDTO;
    }




    public LancamentoDTO lancar(ContaContabil contaOrigem, TipoLancamento tipoOrigem,
                ContaContabil contaDestino, TipoLancamento tipoDestino,
        double valor) {
        LancamentoDTO lancamentoDTO = new LancamentoDTO();

        LancamentoContabil lancamentoOrigem = new LancamentoContabil();
        lancamentoOrigem.setConta(contaOrigem);
        lancamentoOrigem.setValor(valor);
        lancamentoOrigem.setTipo(tipoOrigem);
        contaOrigem.getLancamentos().add(lancamentoOrigem);
        lancamentoRepository.save(lancamentoOrigem);
        contaRepository.save(contaOrigem);

        lancamentoDTO.setLancamentoOrigem(lancamentoOrigem);

        LancamentoContabil lancamentoDestino = new LancamentoContabil();
        lancamentoDestino.setConta(contaDestino);
        lancamentoDestino.setValor(valor);
        lancamentoDestino.setTipo(tipoDestino);
        contaOrigem.getLancamentos().add(lancamentoDestino);
        lancamentoRepository.save(lancamentoDestino);
        contaRepository.save(contaDestino);

        lancamentoDTO.setLancamentoDestino(lancamentoDestino);

        return lancamentoDTO;
    }
}
