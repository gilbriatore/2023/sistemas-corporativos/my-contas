package briatore.services;

import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoConta;
import briatore.entities.TipoLancamento;
import briatore.repositories.ContaContabilRepository;
import briatore.repositories.LancamentoContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


@Service
public class DreService {

    @Autowired
    private ContaContabilRepository contaRepository;

    @Autowired
    private LancamentoContabilRepository lancamentoRepository;

    public Double calcularReceitas() {
        List<ContaContabil> contas = contaRepository.findByTipoIn(List.of(TipoConta.RECEITA));
        List<LancamentoContabil> lancamentos = lancamentoRepository.findByTipoAndContaIn(TipoLancamento.CREDITO, contas);
        Double receitas = 0.0;
        for (LancamentoContabil lancamento : lancamentos) {
            receitas += lancamento.getValor();
        }
        return receitas;
    }

    public Double calcularDespesas() {
        List<ContaContabil> contas = contaRepository.findByTipoIn(List.of(TipoConta.DESPESA));
        List<LancamentoContabil> lancamentos = lancamentoRepository.findByTipoAndContaIn(TipoLancamento.DEBITO, contas);
        Double despesas = 0.0;
        for (LancamentoContabil lancamento : lancamentos) {
            despesas += lancamento.getValor();
        }
        return despesas;
    }

    public Double calcularResultadoLiquido() {
        Double receitas = calcularReceitas();
        Double despesas = calcularDespesas();
        return receitas - despesas;
    }
}