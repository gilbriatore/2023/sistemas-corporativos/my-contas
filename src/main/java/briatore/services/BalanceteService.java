package briatore.services;

import briatore.entities.ContaContabil;
import briatore.entities.LancamentoContabil;
import briatore.entities.TipoConta;
import briatore.entities.TipoLancamento;
import briatore.repositories.ContaContabilRepository;
import briatore.repositories.LancamentoContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BalanceteService {
    @Autowired
    private ContaContabilRepository contaRepository;

    @Autowired
    private LancamentoContabilRepository lancamentoRepository;

    @Autowired
    private DreService dreService;

    public Map<ContaContabil, Double> calcularSaldos() {
        Map<ContaContabil, Double> saldos = new HashMap<>();
        List<ContaContabil> contas = contaRepository.findByTipoIn(Arrays.asList(TipoConta.ATIVO, TipoConta.PASSIVO));
        for (ContaContabil conta : contas) {
            List<LancamentoContabil> lancamentos = lancamentoRepository.findByConta(conta);
            Double saldo = 0.0;
            for(LancamentoContabil lancamento : lancamentos) {
                if(TipoLancamento.DEBITO.equals(lancamento.getTipo())) {
                    saldo -= lancamento.getValor();
                }else {
                    saldo += lancamento.getValor();
                }

            }

            //Se for a conta de resultados
            if (conta.getCodigo().equals("2.3.1.1.01")) {
                Double saldoDRE = dreService.calcularResultadoLiquido();
                //Faz o lançamento de lucro ou prejuízo
                if (saldoDRE >= 0) {
                    saldo += saldoDRE;
                }  else {
                    saldo -= saldoDRE;
                }
            }

            saldos.put(conta, saldo);
        }
        return saldos;
    }
}
