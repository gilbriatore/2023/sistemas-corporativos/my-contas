package briatore.entities;

public enum TipoConta {
    ATIVO,
    PASSIVO,
    DESPESA,
    RECEITA
}