package briatore.entities;

public enum TipoLancamento {
    DEBITO,
    CREDITO
}