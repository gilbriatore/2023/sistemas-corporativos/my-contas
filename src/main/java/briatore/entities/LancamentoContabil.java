package briatore.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "lancamentos_contabeis")
@Data
public class LancamentoContabil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "conta_id", nullable = false)
    private ContaContabil conta;

    @Column(nullable = false)
    private double valor;

    @Enumerated(EnumType.STRING)
    private TipoLancamento tipo;
}
