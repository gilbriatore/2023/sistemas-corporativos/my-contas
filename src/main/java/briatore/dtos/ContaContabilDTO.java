package briatore.dtos;

import briatore.entities.ContaContabil;
import lombok.Data;

@Data
public class ContaContabilDTO {

    private ContaContabil contaContabil;
    private double saldo;


    public void setContaContabil(ContaContabil contaContabil) {
        this.contaContabil = contaContabil;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
